# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api

class chilean_invoice_setting(models.Model):
    
    _name = "chilean.config.settings"
    
    connect_chilean_invoice = fields.Boolean("Connect Chilean Invoice")
    api_url = fields.Char('Api URL',default='http://libredte.redcomercio.cl')
    api_key = fields.Char("Api Key")
    
    @api.multi
    def write(self,vals):
        if vals.has_key('connect_chilean_invoice'):
            all_inv = self.env['account.invoice'].search([('type','=','out_invoice')])
            for inv in all_inv:
                ans = inv.write({'is_chilean_check':vals['connect_chilean_invoice']})
        res = super(chilean_invoice_setting,self).write(vals)
        return res
        
    
