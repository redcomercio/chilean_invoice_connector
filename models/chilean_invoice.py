# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api,_
from os import sys
import requests, json
from odoo.exceptions import UserError, ValidationError

TYPE2SIICODE = {
    'out_invoice': 33,  # Customer Invoice
    'in_invoice': 33,   # Vendor Bill
    'out_refund': 61,   # Customer Refund
    'in_refund': 61,    # Vendor Refund
}

CODREFNOTACREDITO = {
    'ANULA DOCUMENTO': 1, 
    'CORRIGE TEXTO': 2,
    'CORRIGE MONTO': 3,
}

class LibreDTE:

    def __init__ (self, hash, url = 'https://libredte.cl'):
        self.url = url
        self.hash = hash 
        
    def get (self, api) :
        headers = {'authorization':self.hash}
        return requests.get(self.url+'/api'+api,headers=headers)

    def post (self, api, data = None) :
        if isinstance(data, str) :
            data_json = data
        else :
            data_json = json.dumps(data)
        headers = {'authorization':self.hash}
        return requests.post(self.url+'/api'+ api, data_json,headers=headers )
    
class connect_chilean_to_invoice(models.Model):
    
    _inherit = 'account.invoice'
    
    @api.model
    def is_chilean(self):
        all_url = self.env['chilean.config.settings'].search([])
        connect = False
        for i in all_url:
            return i.connect_chilean_invoice
        return connect
    
    emitor_data = fields.Char('Emitor Data',)
    folio = fields.Char('Folio Number')
    xml = fields.Text("Chilean XML")
    is_chilean_check = fields.Boolean("Is Chilean",default=is_chilean)
    attachment_ids = fields.One2many('ir.attachment', 'res_id', domain=[('res_model', '=', 'account.invoice')], string='Attachments')
    attachment_number = fields.Integer(compute='_get_attachment_number', string="Number of Attachments")
    
    @api.multi
    def _get_attachment_number(self):
        read_group_res = self.env['ir.attachment'].read_group(
            [('res_model', '=', 'account.invoice'), ('res_id', 'in', self.ids)],
            ['res_id'], ['res_id'])
        attach_data = dict((res['res_id'], res['res_id_count']) for res in read_group_res)
        for record in self:
            record.attachment_number = attach_data.get(record.id, 0)
    
    @api.multi
    def action_get_attachment_tree_view(self):
        attachment_action = self.env.ref('base.action_attachment')
        action = attachment_action.read()[0]
        action['context'] = {'default_res_model': self._name, 'default_res_id': self.ids[0]}
        action['domain'] = str(['&', ('res_model', '=', self._name), ('res_id', 'in', self.ids)])
        return action 

    def get_url(self):
        all_url = self.env['chilean.config.settings'].search([])
        url = False
        for i in all_url:
            return str(i.api_url)
        return url
    
    def get_key(self):
        all_url = self.env['chilean.config.settings'].search([])
        key = False
        for i in all_url:
            return str(i.api_key)
        return key
    
    def set_date_invoice(self):
        if self.date_invoice:
            return self.date_invoice
        else:
            return fields.Date.context_today(self)

    def set_date_due(self,date_invoice):
        if self.date_due:
            return self.date_due 
        else:
            if not self.payment_term_id:
                self.date_due = self.date_due or self.date_invoice or fields.Date.context_today(self)
            else:
                pterm = self.payment_term_id
                pterm_list = pterm.with_context(currency_id=self.company_id.currency_id.id).compute(value=1, date_ref=date_invoice)[0]
                self.date_due = max(line[0] for line in pterm_list)
            return self.date_due

    def set_billing_period_start(self):
        if self.billing_period_start:
            return self.billing_period_start
    def set_billing_period_end(self):
        if self.billing_period_end:
            return self.billing_period_end

    def emitir_dte(self):
        vals = {}
        if (not self.move_name) and (self.type == 'out_invoice' or self.type == 'out_refund') and self.state == 'draft' and self.is_chilean_check:
            company = self.env['res.users'].browse(self.env.uid).company_id
            url = self.get_url()
            hash = self.get_key()
            if not url:
                raise ValidationError("Verifique la URL ingresada en las configuraciones de chilean invoice connector")
            if not hash:
                raise ValidationError("Verifique el APIKEY ingresada en las configuraciones de chilean invoice connector")
            Cliente = LibreDTE(hash,url)
            
            iddoc = {"TipoDTE": TYPE2SIICODE[self.type]}

            date_invoice = self.set_date_invoice()
            iddoc.update({"FchEmis": date_invoice})
            iddoc.update({"FchVenc":self.set_date_due(date_invoice)})
            if self.set_billing_period_start():
                iddoc.update({"PeriodoDesde": self.set_billing_period_start()})
            if self.set_billing_period_end():
                iddoc.update({"PeriodoHasta":self.set_billing_period_end()})
            iddoc.update({"TermPagoGlosa": "Recuerde que puede pagar con transferencia o deposito cta. cte. "+str(company.partner_id.bank_ids[0].acc_number)+" "+company.partner_id.bank_ids[0].bank_name})     if company.partner_id.bank_ids[0] else False 

            receptor = {"RznSocRecep": str(self.partner_id.name),}
            
            if self.partner_id.vat:
                c_vat = self.partner_id.vat.replace("-","").replace(".","")
                c_vat = c_vat[2:-1] + '-' + c_vat[-1]
                receptor.update({"RUTRecep":str(c_vat)})
            else:
                raise ValidationError("No está definido el RUTRecep(VAT) del cliente, agreguelo.")
            if self.partner_id.partner_turn:
                receptor.update({"GiroRecep": self.partner_id.partner_turn})
            if self.partner_id.phone:
                receptor.update({"Contacto":str(self.partner_id.phone)})
            if self.partner_id.email:
                receptor.update({"CorreoRecep": self.partner_id.email})
            if self.partner_id.street:
                receptor.update({"DirRecep": self.partner_id.street})
            if self.partner_id.street2:
                receptor.update({"CmnaRecep": self.partner_id.street2})
            
            Detalle = []
            
            if self.invoice_line_ids:
                for inv in self.invoice_line_ids:
                    prod_data = {}
                    prod_data['NmbItem'] = inv.product_id.name
                    prod_data['QtyItem'] = inv.quantity
                    prod_data['PrcItem'] = inv.price_unit
                    if inv.product_id.default_code:
                        prod_data.update({"CdgItem":str(inv.product_id.default_code)})
                    if inv.name:
                        prod_data.update({"DscItem":inv.name})
                    if inv.discount:
                        prod_data.update({"DescuentoPct":inv.discount})
                    Detalle.append(prod_data)
            
            company_vat = company.vat
            if not company_vat:
                raise ValidationError("No está definido el RUT de la compañia emisora.")
            else:
                company_vat = company_vat.replace("-","").replace(".","")
                company_vat = company_vat[2:-1] + '-' + company_vat[-1:]
            
            dte = {
               "Encabezado": 
               {
                    "IdDoc": iddoc,
                    "Emisor": {"RUTEmisor": str(company_vat)},
                    "Receptor": receptor,
                },
                   "Detalle": Detalle
               }

            if self.type == 'out_refund' :   
                origin = self.search([('type', '=', "out_invoice"), ('company_id', '=', self.company_id.id), ('number', '=', self.origin)])
                if not self.name.upper() in CODREFNOTACREDITO:
                    raise ValidationError("Porfavor escriba una de las siguientes alternativas en el motivo: Anula documento, Corrige monto o Corrige texto")
                Referencia = {
                    "NroLinRef" : 1,
                    "TpoDocRef" : TYPE2SIICODE[origin.type],
                    "FolioRef" : origin.folio,
                    "FchRef" : origin.date_invoice,
                    "CodRef" : CODREFNOTACREDITO[self.name.upper()],
                    "RazonRef" : self.name
                }
                dte.update({"Referencia": Referencia})
                
            if dte:
                emitir = Cliente.post('/dte/documentos/emitir', dte)
                if emitir:
                    try:
                        vals['emitor_data'] = emitir.json()
                    except Exception:
                        raise ValidationError("Error al emitir DTE temporal with json response json code:"+str(emitir.status_code))
                else:
                    raise ValidationError(emitir.text+' '+str(emitir.status_code))
            else:
                raise ValidationError("Dte Not defined.Please add Dte.")
        return vals    
    @api.multi
    def action_invoice_open(self):
        is_chilean = self.is_chilean_check
        if (not self.move_name) and (self.type == 'out_invoice' or self.type == 'out_refund') :
            if is_chilean:
                emitor = self.emitir_dte()
                url = self.get_url()
                if not url:
                    raise ValidationError("Please Provide the url to connect to chilean invoice")
                hash = self.get_key()
                if not hash:
                    raise ValidationError("Please Provide the Key to connect to chilean invoice")
                
                Cliente = LibreDTE(hash,url)
                if emitor['emitor_data']:
                    emitir = Cliente.post('/dte/documentos/generar?getXML=true', emitor['emitor_data'])
                    if emitir:
                        try:
                            emitir_data = emitir.json()
                            xml = {'xml':emitir_data['xml'],
                                   "cedible": 0,
                                    "papelContinuo": 0,
                                    "compress": False,
                                    "copias_tributarias": 1,
                                    "copias_cedibles": 0}
                            pdf = Cliente.post('/utilidades/documentos/generar_pdf',xml)
                            if self.date_invoice:
                                att_val = {
                                   'name' : 'chilean-' + self.partner_id.name + str(self.date_invoice)+'.pdf',
                                   'type' : 'binary',
                                   'db_datas' : pdf.content.encode('base64'),
                                   'datas' : pdf.content.encode('base64'),
                                   'res_model' :'account.invoice',
                                   'datas_fname' : 'chilean-' + self.partner_id.name + str(self.date_invoice)+'.pdf',
                                   'mimetype' : 'application/pdf'
                                   } 
                            else:
                                att_val = {
                                   'name' : 'chilean-' + self.partner_id.name + '.pdf',
                                   'type' : 'binary',
                                   'db_datas' : pdf.content.encode('base64'),
                                   'datas' : pdf.content.encode('base64'),
                                   'res_model' :'account.invoice',
                                   'datas_fname' : 'chilean-' + self.partner_id.name + '.pdf',
                                   'mimetype' : 'application/pdf'
                                }
                            ans = self.write({'xml':emitir_data['xml'],'folio':emitir_data['folio'],'attachment_ids':[(0,0,att_val)]})
                        except Exception,e:
                            raise ValidationError("Missing some emitir data with json status code "+str(emitir.status_code))
                    else:
                        raise ValidationError(emitir.text+' '+str(emitir.status_code))
                else:
                    raise ValidationError("There is not Emitor Data on invoice creation.Please add emitor data to get Folio number")
        res = super(connect_chilean_to_invoice,self).action_invoice_open()
        return res

    @api.multi
    def send_document(self):
        for doc in self.attachment_ids:
            if doc.name.find(self.partner_id.name) >= 0:
                email_to = self.partner_id.email
                mail_mail = self.env['mail.mail']
                data = {
                                    'recv' : self.partner_id.name,
                                    'number': self.number,
                                    'company' :self.env.user.company_id.name or '',
                                    'amount' : self.amount_total
                                    }
                
                WELCOME_EMAIL_BODY = _("""Dear %(recv)s,
                        
Here is your Chilean invoice %(number)s amounting in 60000.0 INR from YourCompany. <br/><br/>
Please remit payment at your earliest convenience.

Thank you,

%(company)s
""")
                mail_values = {
                'email_from': self.env.user.email,
                'email_to': email_to,
                'subject': (_("Chilen Invoice order %(number)s") %data),
                'body_html': '<pre>%s</pre>' % (_(WELCOME_EMAIL_BODY) % data),
                'state': 'outgoing',
                'attachment_ids' : [(6,0,[doc.id])]
                }                
                mail_id = mail_mail.create(mail_values)
                ans = mail_mail.send([mail_id])
        return True

class add_turn(models.Model):
    
    _inherit = 'res.partner'
    
    partner_turn = fields.Char("Partner Turn")
    
    
class add_billing_period(models.Model):
    
    _inherit = 'account.invoice'
    
    billing_period_start = fields.Date(string='Billing Perdiod Start',
        readonly=True, states={'draft': [('readonly', False)]}, index=True,
        help="Agregue para definir periodo de facturación", copy=False)
    billing_period_end = fields.Date(string='Billing Perdiod End',
        readonly=True, states={'draft': [('readonly', False)]}, index=True,
        help="Agregue para definir fin del periodo de facturación", copy=False)