# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Chilean Invoice Connector',
    'version' : '1.1',
    'summary': 'Chilean Invoice Connector',
    'sequence': 32,
    'description': """
Chilean Invoice Connector
=========================
This module is used to Connect Chilean invoice to odoo.
    """,
    'category': 'Invoicing',
    'website': 'https://www.justcodify.com/',
    'author' : 'Just Codify',
    'images' : [],
    'depends' : ['base_vat'],
    'data': [
             'security/ir.model.access.csv',
             'views/chilean_invoice_config_view.xml',
             'views/chilean_invoice_view.xml',
             'data/chilean_configuartion_data.xml',
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'installable': True,
    'auto_install': False,
}